package base;


import base.display.DisplayManager;
import base.display.TextDisplayManager;
import base.gamers.Computer;
import base.gamers.Gamer;
import base.gamers.Human;
import base.keyboard.KeyboardManager;
import base.move.ComputerMove;
import base.move.HumanMove;
import base.move.Move;

import java.util.List;

public class GameManager {

    private Board board;

    private Gamer computer;
    private Gamer human;

    private final DisplayManager displayManager = new TextDisplayManager();
    private final KeyboardManager keyboardManager = new KeyboardManager(this);
    private final StaticsController statisticsCtrl = new StaticsController();

    void initialize() {
        board = new Board(Options.fieldSize);
        human = new Human(keyboardManager);
        computer = new Computer(board);
    }

    void start() {
        while (true) {
            Move computerMove = computer.makeMove();
            applyMove(computerMove);
            displayBoard();
            Move humanMove = human.makeMove();
            if (humanMove != null)
                applyMove(humanMove);
            else {
                break;
            }
            displayBoard();
        }
        finishGame();
    }

    private void displayBoard() {
        if (board != null)
            displayManager.showBoard(board);
    }

    private void finishGame() {
        System.out.println("Game finished!");
        statisticsCtrl.displayGameResults();
    }

    private void applyMove(Move move) {
        if (move == null) return;
        if (move instanceof ComputerMove) {
            applyComputerMove((ComputerMove) move);
        } else if (move instanceof HumanMove) {
            applyHumanMove((HumanMove) move);
        }
    }

    private void applyComputerMove(ComputerMove move) {
        statisticsCtrl.newChipCreated(move.getChip());
        System.out.println("computer move: (" + move.getPosX() + ", " + move.getPosY() + ")");
        board.modify(move.getPosX(), move.getPosY(), move.getChip());
    }

    private void applyHumanMove(HumanMove move) {
        HumanMove.Direction dir = move.getDir();
        System.out.println("human move: " + dir);
        int reduceDirection = (dir == HumanMove.Direction.UP || dir == HumanMove.Direction.LEFT) ? 1 : -1;
        switch (dir) {
            case UP:
            case DOWN:
                for (int i = 0; i < board.getWidth(); i++) {
                    List<Chip> list = board.getChipsFromCol(i);
                    reduceList(list, reduceDirection);
                    board.setChipsForCol(i, list, reduceDirection);
                }
                break;
            case LEFT:
            case RIGHT:
                for (int i = 0; i < board.getHeight(); i++) {
                    List<Chip> list = board.getChipsFromRow(i);
                    reduceList(list, reduceDirection);
                    board.setChipsForRow(i, list, reduceDirection);
                }
                break;
        }
    }

    /**
     * Reduces the list of chips
     * @param chips
     * @param direction left(1), right(-1)
     */
    private void reduceList(List<Chip> chips, int direction) {
        if (chips.isEmpty()) return;
        boolean changed = true;
        while (changed) {
            changed = false;
            int startIdx = (direction == 1) ? 0 : chips.size() - 1;
            int endIdx = (direction == 1) ? chips.size() - 1 : 0;
            for (int j = startIdx; j != endIdx; j += direction) {
                if (chips.get(j).getValue() == chips.get(j+direction).getValue()) {
                    int oldVal = chips.get(j).getValue();
                    Chip newChip = new Chip(oldVal * 2);
                    statisticsCtrl.newChipCreated(newChip);
                    chips.set(j, newChip);
                    chips.remove(j+direction);
                    changed = true;
                    break;
                }
            }
        }
    }
}
