package base;

import base.collections.Pair;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private final int height;
    private final int width;
    private final Chip[][] board;

    Board(int height, int width) {
        this.height = height;
        this.width = width;
        this.board = new Chip[height][width];
    }

    Board(int size) {
        this.width = size;
        this.height = size;
        this.board = new Chip[getHeight()][getWidth()];
    }

    public Chip getChip(int x, int y) {
        return board[x][y];
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public List<Pair<Integer, Integer>> getEmptyCoords() {
        List<Pair<Integer, Integer>> l = new ArrayList<>(height * width);
        for (int i = 0; i < height; i++){
            for (int  j = 0; j < width; j++){
                if (board[i][j] == null){
                    l.add(new Pair<>(i, j));
                }
            }
        }
        return l;
    }

    public List<Chip> getChipsFromRow(int rowIdx) {
        List<Chip> l = new ArrayList<>();
        for (int i = 0; i < width; i++){
            if (board[rowIdx][i] != null){
                l.add(board[rowIdx][i]);
            }
        }
        return l;
    }

    public List<Chip> getChipsFromCol(int colIdx) {
        List<Chip> l = new ArrayList<>();
        for (int i = 0; i < height; i++){
            if (board[i][colIdx] != null){
                l.add(board[i][colIdx]);
            }
        }
        return l;
    }

    /**
     * @param direction left (1), right (-1)
     */
    public void setChipsForRow(int rowIdx, List<Chip> chips, int direction) {
        for (int i = 0; i < width; i++){
            board[rowIdx][i] = null;
        }
        int startPos = (direction == 1) ? 0 : width - chips.size();
        for (int i = 0; i < chips.size(); i++){
            board[rowIdx][startPos + i] = chips.get(i);
        }
    }

    /**
     * @param direction left (1), right (-1)
     */
    public void setChipsForCol(int colIdx, List<Chip> chips, int direction) {
        for (int i = 0; i < width; i++){
            board[i][colIdx] = null;
        }
        int startPos = (direction == 1) ? 0 : width - chips.size();
        for (int i = 0; i < chips.size(); i++){
            board[startPos + i][colIdx] = chips.get(i);
        }
    }

    void modify(int x, int y, Chip newChip) {
        board[x][y] = newChip;
    }
}
