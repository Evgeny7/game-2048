package base;

public class Chip {

    private final int value;

    public Chip(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
         return Integer.toString(value);
    }

}
