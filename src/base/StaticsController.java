package base;

/**
 * Created by Женя on 26.02.2017.
 */
public class StaticsController {

    private int score = 0;

    public void newChipCreated(Chip chip) {
        score += chip.getValue();
    }

    public void displayGameResults() {
        System.out.println("Game results: ");
        System.out.println(score + " scores");
    }
}
