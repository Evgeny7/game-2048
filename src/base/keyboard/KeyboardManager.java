package base.keyboard;

import base.GameManager;
import base.move.HumanMove;
import base.move.Move;

import java.io.IOException;

public class KeyboardManager {

    private final GameManager gameManager;

    public KeyboardManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    public Move getHumanMove() {
        Character cc = getChar();
        if (cc == null) {
            // reading error
            System.err.print("Error while reading System.in");
            return null;
        }
        char c = cc;
        while (true) {
            if (c == 'w' || c == 's' || c == 'a' || c == 'd' || c == 'v') {
                switch (c) {
                    case 'w':
                        return new HumanMove(HumanMove.Direction.UP);
                    case 's':
                        return new HumanMove(HumanMove.Direction.DOWN);
                    case 'a':
                        return new HumanMove(HumanMove.Direction.LEFT);
                    case 'd':
                        return new HumanMove(HumanMove.Direction.RIGHT);
                    case 'v':
                        //gameManager.finishGame();
                    default:
                        return null;
                }
            }
            System.out.println("invalid input");
            c = getChar();
        }
    }

    private Character getChar() {
        try {
            char c = (char)System.in.read();
            while(System.in.read() != '\n') {}
            return c;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

}
