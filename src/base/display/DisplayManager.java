package base.display;

import base.Board;

public interface DisplayManager {

    void showBoard(Board board);
}
