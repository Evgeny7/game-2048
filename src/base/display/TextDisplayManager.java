package base.display;

import base.Board;
import base.Chip;

public class TextDisplayManager implements DisplayManager {

    @Override
    public void showBoard(Board board) {
        int h = board.getHeight();
        int w = board.getWidth();

        System.out.println("-----------------------------------------");
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                Chip c = board.getChip(i,j);
                if (c == null)
                    System.out.print("_\t");
                else
                    System.out.print(c + "\t");

            }
            System.out.println();
        }
        System.out.println("-----------------------------------------");
    }
}
