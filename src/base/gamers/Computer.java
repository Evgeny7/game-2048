package base.gamers;

import base.Board;
import base.Chip;
import base.move.ComputerMove;
import base.move.Move;
import base.collections.Pair;

import java.util.List;
import java.util.Random;

public class Computer implements Gamer {

    private final Board board;
    private final Random random = new Random(System.currentTimeMillis());

    public Computer(Board board) {
        this.board = board;
    }

    @Override
    public Move makeMove() {
        List<Pair<Integer, Integer>> emptyCoords = board.getEmptyCoords();
        int index = random.nextInt(emptyCoords.size());
        Pair<Integer, Integer> coord = emptyCoords.get(index);
        Chip newChip = new Chip(2);
        return new ComputerMove(coord.getFirst(), coord.getSecond(), newChip);
    }
}
