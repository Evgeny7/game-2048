package base.gamers;

import base.keyboard.KeyboardManager;
import base.move.Move;

public class Human implements Gamer {

    private final KeyboardManager keyboardManager;

    public Human(KeyboardManager keyboardManager) {
        this.keyboardManager = keyboardManager;
    }

    @Override
    public Move makeMove() {
        return keyboardManager.getHumanMove();
    }
}
