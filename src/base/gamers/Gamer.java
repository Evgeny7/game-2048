package base.gamers;

import base.move.Move;

public interface Gamer {

    public Move makeMove();
}
