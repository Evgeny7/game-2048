package base.move;

public class HumanMove implements Move {

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public Direction getDir() {
        return dir;
    }

    private final Direction dir;

    public HumanMove(Direction dir) {
        this.dir = dir;
    }

}
