package base.move;

import base.Chip;

public class ComputerMove implements Move {

    private final int posX;
    private final int posY;
    private final Chip chip;

    public ComputerMove(int posX, int posY, Chip chip) {
        this.posX = posX;
        this.posY = posY;
        this.chip = chip;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public Chip getChip() {
        return chip;
    }
}
